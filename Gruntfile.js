module.exports = function (grunt) {

	grunt.initConfig({
		// Metadata.
		pkg: grunt.file.readJSON('package.json'),
		datetime: Date.now(),

		bower: {
			install: {
				options: {
					targetDir: "./src",
					layout: 'byType',
					install: true,
					verbose: false,
					cleanTargetDir: false,
					cleanBowerDir: false,
					bowerOptions: {}
				}
			}
		},

		copy: {
			main: {
				files: [
					{
						expand: true,
						flatten: true,
						filter: 'isFile',
						cwd: 'src/components-bootstrap/less/',
						src: ['*.less'],
						dest: 'css/bootstrap/'
					},
					{
						expand: true,
						flatten: true,
						filter: 'isFile',
						cwd: 'src/jquery/dist/',
						src: ['**'],
						dest: 'js/'
					},
					{
						expand: true,
						flatten: true,
						filter: 'isFile',
						cwd: 'src/font-awesome/less/',
						src: ['*.less'],
						dest: 'css/font-awesome/'
					},
					{
						expand: true,
						flatten: true,
						filter: 'isFile',
						cwd: 'src/font-awesome/fonts/',
						src: ['**'],
						dest: 'fonts/'
					}
				]
			}
		}
	});

	grunt.loadNpmTasks('grunt-bower-task');
	grunt.loadNpmTasks('grunt-contrib-copy');

	//tasks
	grunt.registerTask('default', ['bower', 'copy']);

	grunt.registerTask('init', []);

};